Provides a few simple VIs that help interact with currently installed LabVIEW IDEs.

# VI List

#### Installed LabVIEW Versions.vi 

List information about currently installed LabVIEW versions.

#### Open LabVIEW App ref.vi

Opens a reference to the specified LabVIEW version. If the LabVIEW dev environment isn't currently running one will be launched.

## Contributing

See [Contributing.md](CONTRIBUTING.md) for information on how to submit pull requests. Bugs can be reported using the repositories issue tracker.

#### _This package is implemented with LabVIEW 2019_
